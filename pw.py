#! python3
# pw.py - Andy's insecure password manager

# ADDED FEATURE:
# Password manager adds creation time to entry into the database when providing
# user with text to enter into the database.  Also the database is a separate
# file named 'password_database.py'.  When retrieving the password from the
# database, whether it is printing it or copying it to the clipboard, it will
# inform the user the time difference between the current time and the time
# that the user put the password into the database.  Entering no arguments
# will display the help documentation.

"""Andy's insecure password manager."""
import sys
import time
import math
import pyperclip
import decrypt_shift
import encrypt_shift
import password_database
import xkcd_pw_gen


HELP_TEXT = """Usage: python pw.py [flag] [account] [password]

Flags
copy    - copies the password to the clipboard
create  - create XKCD-style passphrase
get     - prints password (account required)
help    - show this help text
store   - copies an encrypted entry for the database (account & pw required)
"""


def time_dif(old_time):
    """Get time since old_time from largest nonzero time units to smallest."""
    current_time = time.time()
    timedif = current_time - old_time
    time_dif_seconds = math.floor(abs(timedif))
    time_dif_minutes = math.floor(time_dif_seconds / 60)
    time_dif_hours = math.floor(time_dif_minutes / 60)
    time_dif_days = math.floor(time_dif_hours / 24)

    time_dif_extra_hours = time_dif_hours - (time_dif_days * 24)
    time_dif_extra_minutes = time_dif_minutes - (time_dif_hours * 60)
    time_dif_extra_seconds = time_dif_seconds - (time_dif_minutes * 60)

    return_string = ''

    if time_dif_days >= 1:
        return_string += str(time_dif_days) + ' day(s), '
    if time_dif_extra_hours >= 1:
        if time_dif_days >= 1 and \
           (time_dif_extra_minutes <= 0 or time_dif_extra_seconds <= 0):
            return_string += ', and '
        return_string += str(time_dif_extra_hours) + ' hour(s)'
        if time_dif_extra_minutes > 0 or time_dif_extra_seconds > 0:
            return_string += ', '
    if time_dif_extra_minutes >= 1:
        if time_dif_extra_seconds <= 0:
            return_string += 'and '
        return_string += str(time_dif_extra_minutes) + ' minute(s)'
        if time_dif_extra_seconds > 0:
            return_string += ', '
    if time_dif_extra_seconds >= 1:
        return_string += 'and ' + str(time_dif_extra_seconds) + ' second(s)'

    if timedif > 0:
        return_string += ' ago'
    else:
        return_string += ' in the future'

    return return_string


def get_password(account):
    """Print the password of account."""
    key = int(input('Please enter your key:  '))
    if account in password_database.PASSWORDS:
        p_password = decrypt_shift.\
                     decrypt_string(password_database.
                                    PASSWORDS[account]['pw'], key)
        print('The password for', account, 'is:')
        print(p_password)
        print('This password was created',
              time_dif(password_database.PASSWORDS[account]['created']))


def copy_password(account):
    """Copy the password of account to the clipboard."""
    key = int(input('Please enter your key:  '))
    if account in password_database.PASSWORDS:
        p_password = decrypt_shift.decrypt_string(password_database.
                                                  PASSWORDS[account]['pw'],
                                                  key)
        pyperclip.copy(p_password)
        print('Password for', account, 'copied to clipboard.')
        print('This password was created ',
              time_dif(password_database.PASSWORDS[account]['created']))
    else:
        print('There is no account named', account)


def store_password(p_account, p_password):
    """Copy an encrypted entry of account and password to the clipboard."""
    key = int(input('Enter your key:  '))
    e_password = encrypt_shift.encrypt_string(p_password, key)
    storage_string = "'" + p_account + "': {'pw': '" + e_password \
                     + "', 'created': " + str(math.floor(time.time())) + "},"
    pyperclip.copy(storage_string)
    print("""Copied database entry to clipboard.
Please paste it onto the third line from the end.""")


# print help text if no arguments are given
if len(sys.argv) == 1:
    print(HELP_TEXT)
    sys.exit()

# help flag is used to show help text
if sys.argv[1] == 'help':
    print(HELP_TEXT)
    sys.exit()

# create flag is used to generate xkcd-style passphrases
if sys.argv[1] == 'create' and len(sys.argv) == 3:
    pyperclip.copy(xkcd_pw_gen.xkcd_pw_gen())
    print('Created password for', sys.argv[2],
          'and copied it to the clipboard')
    sys.exit()

# copy flag is used to copy password to clipboard
if sys.argv[1] == 'copy' and len(sys.argv) == 3:
    copy_password(sys.argv[2])
    sys.exit()
elif len(sys.argv) < 3:
    print('Please specify an account')
    sys.exit()

# get flag is used to print password
if sys.argv[1] == 'get' and len(sys.argv) == 3:
    get_password(sys.argv[2])
    sys.exit()
elif len(sys.argv) < 3:
    print('Please specify an account')
    sys.exit()

# store flag is used to encrypt an account and password for entry into the
# password password_database
if sys.argv[1] == 'store' and len(sys.argv) == 4:
    store_password(sys.argv[2], sys.argv[3])
    sys.exit()
elif sys.argv[1] == 'store' and len(sys.argv) < 4:
    print('missing arguments')
    sys.exit()


# Reflect =  Your use of triple quotes made the code very readable
#            Clever use of files rather than lists for storing
#            database like information
# Inquire = Do you have any concerns with passing an unencrypted password as a
#           visual parameter
# Suggest = Possibly consider waiting to ask for the Master password once the
#           user is in the program
# Elevate = Consider offering the ability to delete an account entry so they
#           can recreate the password
