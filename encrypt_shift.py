"""this module encrypts using a substitution cipher."""


from decrypt_shift import UPPERCASE_ALPHABET
from decrypt_shift import LOWERCASE_ALPHABET


def encrypt_string(plaintext, shift_amount):
    """Encrypt a string with shift cipher and a given shift amount."""
    ciphertext = ""
    for i in plaintext:
        ciphertext += part_two_encrypt_char(i, shift_amount)

    return ciphertext


def part_two_encrypt_char(plainchar, shift):
    """Encrypt a char with shift cipher and a given shift amount."""
    # if the character is in upper-case then use UPPERCASE_ALPHABET to encrypt
    if plainchar in UPPERCASE_ALPHABET:
        letter_num = UPPERCASE_ALPHABET.index(plainchar)
        return UPPERCASE_ALPHABET[(letter_num+shift) % len(UPPERCASE_ALPHABET)]

    # if the character is in lower-case then use LOWERCASE_ALPHABET to encrypt
    letter_num = LOWERCASE_ALPHABET.index(plainchar)
    return LOWERCASE_ALPHABET[(letter_num+shift) % len(LOWERCASE_ALPHABET)]


# print(part_two('thequickbrownfoxjumpsoverthelazydog', 13))
